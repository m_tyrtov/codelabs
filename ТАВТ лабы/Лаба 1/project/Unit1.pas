unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Imaging.pngimage,
  Vcl.Menus, Unit2, Vcl.StdCtrls, Vcl.ComCtrls;

type
  TForm1 = class(TForm)
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    bucket: TImage;
    start: TButton;
    speedbar: TTrackBar;
    stop: TButton;
    speed: TLabel;
    bg: TImage;
    water: TImage;
    Timer1: TTimer;
    spdval: TLabel;
    settings: TButton;
    animtimer: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure settingsClick(Sender: TObject);
    procedure startClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure speedbarChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

const
  n = 10;

var
  Form1: TForm1;
  arrBucket: array [1..n] of TImage; //����� ����������
  arrValue: array [1..n] of integer; //����� � ������
  arrColor: array [1..n] of string; //���� � ������
  arrWater: array [1..n] of TImage; //����������
  count: integer;

implementation

{$R *.dfm}

procedure TForm1.settingsClick(Sender: TObject);
begin
  form2.showmodal;
end;

procedure TForm1.speedbarChange(Sender: TObject);
begin
  timer1.Interval:=1000 - ((speedbar.Position-1)*100);
  spdval.Caption:=(inttostr(speedbar.Position));
end;

procedure TForm1.startClick(Sender: TObject);
var
  i: integer;
begin
//  for i := 1 to n do
//    begin
//      arrWater[i].Top:=168;
//      arrValue[i]:=0;
//      arrBucket[i].visible:=true;
//      arrWater[i].visible:=true;
//    end;
//  count:=n;
  timer1.Enabled:=true;
  start.Visible:=false;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var
  i, rnd: integer;
begin
  if count>0 then
    begin
      rnd:=1+random(count);
      inc(arrValue[rnd]);
      arrWater[rnd].Top:=arrWater[rnd].Top-5;
        if arrValue[rnd]=10 then
          begin
            arrWater[rnd].Visible:=false;
            arrBucket[rnd].Visible:=false;
            for i := rnd to count-1 do
              begin
                arrValue[i]:=arrValue[i+1];
                arrBucket[i]:=arrBucket[i+1];
                arrWater[i]:=arrWater[i+1];
              end;
            count:=count-1;
          end;
    end
      else
        begin
          timer1.Enabled:=false;
          showmessage('finish');
          start.Visible:=true;
        end;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  i: integer;
begin
  doublebuffered:=true;
  timer1.Interval:=1000 - ((speedbar.Position-1)*100);
  spdval.Caption:=(inttostr(speedbar.Position));
  arrBucket[1]:=TImage.Create(Self);
  arrBucket[1]:=bucket;
  arrWater[1]:=TImage.Create(Self);
  arrWater[1]:=water;
  arrValue[1]:=0;
  for i := 2 to n do
    begin
      arrBucket[i]:=TImage.Create(Self);
      arrBucket[i].Parent:=bucket.Parent;
      arrBucket[i].Picture.LoadFromFile('colors/bucket'+inttostr(i-1)+'.png');
      arrBucket[i].top:=bucket.top;
      arrBucket[i].left:=(i-1)*bucket.Width;
      arrBucket[i].AutoSize:=true;
      arrWater[i]:=TImage.Create(Self);
      arrWater[i].Picture:=water.Picture;
      arrWater[i].Parent:=water.Parent;
      arrWater[i].top:=water.top;
      arrWater[i].left:=(i-1)*water.Width;
      arrWater[i].SendToBack;
      arrValue[i]:=0;
    end;
  count:=n;
end;

end.
