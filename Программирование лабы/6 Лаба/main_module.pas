﻿program main_module;
uses 
  crt, list_module;

const 
  enter = #13;
  bksp = #8;  
  tab = #9;
  up = #038;
  down = #040;
  n = 8;
  max = 5;

var
  cmd: string;
  key: string;
  commandArr: array[1..n] of string;
  commandHistory: array[1..max] of string;
  count: integer;
  
procedure showHelp(); //покажет хелп
begin
  writeln();
  writeln('Системные команды: ');
  writeln('help           Выведет список команд');
  writeln('clrscr         Очистит консоль');
  writeln('exit           Закроет консоль');
  writeln('Команды списка: ');
  writeln('add [-n]       Добавит -n в конец списка');
  writeln('rm [-a]        Удалит элемент с номером -a из списка');
  writeln('show           Выведет список'); 
  writeln('count          Выведет размер списка'); 
  writeln('clear          Очистит список');
  writeln();
  commandArr[1]:='help';
  commandArr[2]:='clrscr';
  commandArr[3]:='exit';
  commandArr[4]:='add';
  commandArr[5]:='rm';
  commandArr[6]:='show';
  commandArr[7]:='count';
  commandArr[8]:='clear';
end;  
  
procedure commandRegex(cmd: string); //обработает команду
var
  unkw: boolean;
  val: longint;
begin
  if length(cmd) <> 0 then
    begin
      unkw:=true;
      if (Regex.IsMatch(cmd, '^help$')) then
        begin
          unkw:=false;
          showHelp();
        end;
      if (Regex.IsMatch(cmd, '^clrscr$')) then
        begin
          unkw:=false;
          clrscr;
        end;
      if (Regex.IsMatch(cmd, '^exit$')) then
        begin
          unkw:=false;
          exit;
        end;
      if (Regex.IsMatch(cmd, '^add \d+$')) then 
        begin
          unkw:=false;
          if length(Regex.Match(cmd, '\d+$').Value)<=10 then addNote(first, getCount(first), StrToInt(Regex.Match(cmd, '\d+$').Value))
            else writeln('Превышена длина допустимого числа');
        end;
      if (Regex.IsMatch(cmd, '^rm \d+$')) then 
        begin
          unkw:=false;
          if length(Regex.Match(cmd, '\d+$').Value)<=10 then
            begin
              val:=StrToInt(Regex.Match(cmd, '\d+$').Value);
              if (getCount(first)>=val) then cut(first, last, val)
                else
                  writeln('Нет такого элемента');
            end
              else
                writeln('Некорректное значение');
        end;
      if (Regex.IsMatch(cmd, '^show$')) then
        begin
          unkw:=false;
          show(first);
        end;
      if (Regex.IsMatch(cmd, '^count$')) then
        begin
          unkw:=false;
          writeln(getCount(first));
        end;
       if (Regex.IsMatch(cmd, '^clear$')) then
        begin
          unkw:=false;
          if getCount(first)>0 then clear(first);
          writeln('Список пуст');
        end;       
      if unkw then
        begin
          writeln('Неизвестная команда или неверно указан параметр');
        end;
    end;
end;  

procedure commandAdd(cmd: string); //запишет команду в историю
var 
  i: integer;
begin
 if count<=max then 
  begin
    if length(trim(cmd))>0 then commandHistory[count]:=trim(cmd);
    inc(count);
  end
    else
      begin
        for i:=1 to max-1 do
          commandHistory[i]:=commandHistory[i+1];
        if length(trim(cmd))>0 then commandHistory[max]:=trim(cmd);         
      end;
end;

procedure commandView(idx: integer);
begin
  cmd:=commandHistory[idx];
end;
 
function commandTab(cmd: string): string; //обработает таб
var
  i, j: integer;
  go: string;
begin
  if (pos('\',cmd)=0) and (pos('[',cmd)=0) then
    begin
      for i:=1 to n do
        if Regex.IsMatch(commandArr[i], '^'+cmd) then
          begin
            inc(j);
            go:=commandArr[i];
          end;
      if j = 1 then commandTab:=go;
    end;
end;  

procedure commandInput(); //обработает ввод
var
  cmdpos: integer;
begin
 cmdpos:=count;
 cmd:='';
	write('>');
	repeat
	  key:=readkey;
	  if key <> enter then
	    begin
	      if ((key <> tab) and (key <> bksp) and (key <> up) and (key <> down) and (length(key)<255)) then cmd:=cmd+key;
        if (key = bksp) then cmd:=copy(cmd, 0, length(cmd)-1);
	      if (key = tab) and ((commandTab(cmd))<>'') then cmd:=commandTab(cmd);
	      if (key = up) then 
	        begin
	          if (cmdpos<>0) then 
	            begin
	              if ((cmdpos-1)<>0) then dec(cmdpos);
                commandView(cmdpos);
	            end
	              else cmd:=copy(cmd, 0, length(cmd)-1);
	        end;
	      if (key = down) then
	        begin
            if (cmdpos<count-1) then
              begin
                inc(cmdpos);
                commandView(cmdpos);
              end
                else cmd:=copy(cmd, 0, length(cmd)-1);
	        end;
	    end;
	  crt.ClearLine;
	  write('>',cmd);
	until key = enter;
	writeln();
end;
   
begin
	clrscr;
  listInit();
  showHelp();
  count:=1;
	repeat
	  commandInput();
	  if length(trim(cmd))<>0 then commandAdd(trim(cmd));
		commandRegex(trim(cmd));
	until (Regex.IsMatch(cmd, '^exit$'));
end.