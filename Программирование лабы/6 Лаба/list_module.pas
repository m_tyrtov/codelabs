﻿unit list_module;

Interface
uses crt;

type
	Tnote = ^note; //plist - ^list
	note = record //list 
    val:  longint; //info
    link: Tnote; //next - plist
  end;

var
	first, last: Tnote;
	idx: longint; //posi
	zn: longint; //zn
  
function  getCount(first: Tnote): longint;
procedure addNote(var first: Tnote; idx: longint; zn: longint);
procedure show(first: Tnote);
procedure cut(var first, last: Tnote; idx: longint);
procedure clear(var first: Tnote);
procedure listInit();

Implementation

procedure listInit();
begin
	first:=nil;
	last:=nil;
end;

function getCount(first: Tnote): longint;
var
	k: longint;
	check: Tnote;
begin
if first=nil then
	k:=0
	else begin
	k:=1;
	check:=first;
	while check^.link<>first do begin
		k:=k+1;
		check:=check^.link;
		end;
	end;
	getCount:=k;
end;

procedure addNote(var first: Tnote; idx: longint; zn: longint);
var
	n,c: longint;
	cur, cikl: Tnote;
begin
n:=getCount(first);
if (first=nil) and (n=0) then begin //Инициализация записи
	new(first);
	first^.val:=zn;
	first^.link:=first;
	last:=first;
	last^.link:=first;
	end
		else if (idx>=1) and (idx<=n) and (first<>nil) and (n=idx) then begin //Добавление за последний элемент
		new(cur);
		cur^.val:=zn;
		cur^.link:=last^.link;
		last^.link:=cur;
		last:=cur;
		last^.link:=cur^.link;
		end
			else if (idx>=1) and (idx<=n) and (first<>nil) and (idx<n) then begin //Добавление где-то в середине списка
			new(cur);
			c:=1;
			cikl:=first;
			while c<idx do begin //Запись нужного элемента в перем-ую
				c:=c+1;
				cikl:=cikl^.link;
			end;
			cur^.val:=zn;
			cur^.link:=cikl^.link;
			cikl^.link:=cur;
			end
end;

procedure show(first: Tnote);
var
	x: Tnote;
	b, m: longint;
begin
m:=getCount(first);
if m=0 then writeln('Список пуст');
x:=first;
b:=1;
while b<(m+1) do begin
	writeln(b,') ',x^.val);
	x:=x^.link;
	b:=b+1;
end;
end;

procedure cut(var first, last: Tnote; idx: longint);
var
	j, i: longint;
	de, fg: Tnote;
begin
i:=getCount(first);
if (idx<1) or (idx>i) then //Проверка вводимого значения
	else if i=0 then writeln('Список пуст')
		else if i=1 then clear(first)
			else if idx=1 then begin //Проверка на удаление первого элемента
			fg:=first;
			first:=first^.link;
			last^.link:=first;
			dispose(fg);
			end
			else if idx=i then begin //Проверка на удаление полседнего элемента
		j:=1;
		de:=first;
		while (j+1)<idx do begin //Запись нужного элемента в перем-ую
			j:=j+1;
			de:=de^.link;
		end;
		fg:=last;
		de^.link:=last^.link;
		dispose(fg);
		last:=de;
		end
		else begin
		j:=1;
		de:=first;
		while (j+1)<idx do begin //Запись нужного элемента в перем-ую
			j:=j+1;
			de:=de^.link;
		end;
		fg:=(de^.link)^.link;
		dispose(de^.link);
		de^.link:=fg;
	end;
end;

procedure clear(var first: Tnote);
var
	kar, karm: Tnote;
begin
if first<>nil then begin
	kar:=first^.link;
	while (kar<>first) do begin
		karm:=kar;
		kar:=kar^.link;
		dispose(karm);
	end;
	dispose(first);
	first:=nil;
	last:=nil;
	end
end;

begin
end.