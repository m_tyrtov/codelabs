﻿program laba5_1;
uses crt;
type 
  arr = array [0..1000000] of longint;
  func = function (a,b: longint): boolean;
var 
  n, i, c: longint; 
  txt: text; 
  mas: arr;

function comp_1(a,b: longint): boolean;
begin
  comp_1:=a > b;
end;

function comp_2(a,b: longint): boolean;
begin
  comp_2:= a < b;
end;

procedure sort(var mas: arr; n: longint; comp: func);
var 
  f: boolean; 
  k,i: longint;
begin
  f:=false;
  while not(f) do
    begin
      f:=true;
      for i:=0 to n-1 do
        if comp(mas[i],mas[i+1])=true then
          begin
            k:=mas[i];
            mas[i]:=mas[i+1];
            mas[i+1]:=k;
            f:=false;
          end;  
    end;
end;

begin
  assign(txt, 'input.txt');
  reset(txt);
  
  Readln(txt, c);
  n:=0;
	while not eoln(txt) do 
  	begin
  		read(txt, mas[n]);
  		inc(n);
  	end;
	dec(n);
  
  sort(mas, n, comp_1);

  assign(txt, 'output.txt');
  rewrite(txt);
  writeln(txt, c);
  for i:=0 to n do
    write(txt, mas[i], ' ');
  close(txt);
end.