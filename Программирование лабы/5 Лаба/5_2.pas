﻿program laba5_2;
uses crt;
type 
  arr = array [0..1000000] of longint;
  func = function (a,b: longint): boolean;
var 
	n, i, c: longint;
	A: arr;
	txt: text;

function comp_1(a,b: longint): boolean; 
begin
  comp_1:=a > b;
end;

function comp_2(a,b: longint): boolean; 
begin
  comp_2:= a < b;
end;

procedure Merge(var A: arr; first, last: longint; comp: func);
var 
	middle, start, final, j: longint;
	mas: arr;
begin
	middle:=(first+last) div 2;
	start:=first; 
	final:=middle+1; 
	for j:=first to last do
		if ((start<=middle) and ((final>last) or (comp(A[start],A[final])))) then
			begin
				mas[j]:=A[start];
				inc(start);
			end
			else
			begin
				mas[j]:=A[final];
				inc(final);
			end;
	for j:=first to last do A[j]:=mas[j];
end;

procedure MergeSort(var A: arr; first, last: longint; comp: func);
begin
	if first<last then
		begin
			MergeSort(A, first, (first+last) div 2, comp); 
			MergeSort(A, (first+last) div 2+1, last, comp);
			Merge(A, first, last, comp);
		end;
end;

begin
	assign(txt, 'input.txt');
  reset(txt);
  
  readln(txt, c);
  n:=0;
	while not eoln(txt) do 
  	begin
  		read(txt, A[n]);
  		inc(n);
  	end;
	dec(n);
  
  MergeSort(A, 0, n, comp_2);

  assign(txt, 'output.txt');
  rewrite(txt);
  writeln(txt, c);
  for i:=0 to n do
    write(txt, A[i], ' ');
  close(txt);
end.